﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class CrossGame : IGame
    {
        private string desc;
        private Player player1,player2;
  
        public CrossGame(string desc, Player player1, Player player2)
        {
            this.desc = desc;
            this.player1 = player1;
            this.player2 = player2;
        }

        public string Info => $"{desc} cross game:{player1.Name} xxx {player2.Name}";
    }
}
