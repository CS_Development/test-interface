﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Player
    {
        private string name;
        public Player(string name)
        {
            this.name = name;
        }
      

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}
