﻿using ConsoleApp7.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            //CrossGame crossGame = new CrossGame("XXX", new Player("Chathea"), new Player("Sok Na"));
            //Console.WriteLine(crossGame.Info);
            GamePlay game = new GamePlay();

            game.PlayGame(new GameA());
            game.PlayGame(new GameB());
            Console.ReadKey();
        }
    }
}
